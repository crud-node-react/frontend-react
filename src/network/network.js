import axios from 'axios';

async function register(user) {
    try {
        let response = await axios.post("http://localhost:4000/registration/insertdata", user);
        console.log("Resp", response)
        return response.data;
    }
    catch (err) {
        return err.response;
    }
}
async function fetchData() {
    try {
        let response = await axios.get("http://localhost:4000/registration/getdata");
        console.log("Resp", response)
        return response.data;
    }
    catch (err) {
        return err.response;
    }
}
async function updateData(user) {
    try {
        console.log('update user', user)
        let response = await axios.put("http://localhost:4000/registration/updatedata", user);
        console.log("Resp", response)
        return response.data;
    }
    catch (err) {
        return err.response;
    }
}
async function deleteUser(id) {
    try {
        let response = await axios.delete("http://localhost:4000/registration/deletedata/" + id);
        console.log("Resp", response)
        return response.data;
    }
    catch (err) {
        return err.response;
    }
}

const netWork = {
    register,
    fetchData,
    updateData,
    deleteUser

}

export default netWork;