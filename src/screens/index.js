import user from './user';
import addUser from './add-user'


const screens = {
    user,
    addUser
 
}

export default screens;
