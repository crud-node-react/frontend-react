import React, { useState, useEffect } from 'react';
import { Row, Col, Table, Form, Button } from 'react-bootstrap';
import netWork from './../../network/network';
import './user.css';

function User(props) {
  const [id, setId] = useState('');
  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [userData, setUserData] = useState([]);
  const [edit, setEdit] = useState(false);
  const [delet, setDelet] = useState(false);

  useEffect(() => {
    fetchUser();
  }, [])

  /**
   * function to call api for insert data into database
   */
  const addUser = async () => {
    let user = { name: name, mobile: phoneNumber };
    // let response = await netWork.register({ user: user });
    let response = await netWork.register(user);
    fetchUser();
  }
  
  /**
   * function to call api for update data of database
   */
  const updateUser = async () => {
    let user = { id: id, name: name, mobile: phoneNumber }
    let response = await netWork.updateData(user);
    setName('');
    setPhoneNumber('');
    setEdit(false);
    fetchUser();
  }
  
  /**
   * function to call api for delete data of database
   */
  const deleteUser = async (user) => {
    console.log("user._id", user._id)
    let response = await netWork.deleteUser(user._id);
    fetchUser();
  }
  
  /**
   * function to call api for fetch database data
   */
  const fetchUser = async () => {
    let response = await netWork.fetchData();
    console.log("response >", response)
    setUserData(response);

  }

  /**
   * function to edit selected data
   */
  const editData = (data) => {
    console.log("ddddd", data)
    setEdit(true);
    setName(data.name);
    setId(data._id);
    setPhoneNumber(data.mobile)
  }

  return (
    <>
      <Row>
        <Col lg={{ span: 4, offset: 4 }}>
          <h3>CRUD Demo</h3>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control type="name" defaultValue={name ? name : ''} onChange={(event) => setName(event.target.value)} placeholder="Enter Name" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control type="name" defaultValue={phoneNumber ? phoneNumber : ''} onChange={(event) => setPhoneNumber(event.target.value)} placeholder="Phone Number" />
            </Form.Group>
            {!edit ?
              <Button variant="primary" onClick={addUser} type="submit">
                Save
              </Button>
              :
              <Button variant="primary" onClick={updateUser} type="submit">
                Update
              </Button>
            }
          </Form>
        </Col>
        <Col lg={12} style={{ marginLeft: "2%", marginTop: "2%", width: "96%" }}>
          <Table striped bordered hover>
            <thead>
              <tr>
                {/* <th>#</th> */}
                <th>Name</th>
                <th>Phone Number</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {userData.map((item) => {
                return (
                  <>
                    <tr>
                      {/* <td>{item._id}</td> */}
                      <td>{item.name}</td>
                      <td>{item.mobile}</td>
                      <td><span className={"edit"} onClick={() => editData(item)}>edit</span>
                        <span onClick={() => deleteUser(item)} className={"delete"}>delete</span></td>
                    </tr>
                  </>
                )

              })}

            </tbody>
          </Table>
        </Col>
      </Row>
    </>

  )

}

export default User;